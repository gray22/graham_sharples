package com.example.sharp.interactivestory.model;

import com.example.sharp.interactivestory.R;

/**
 * Created by sharp on 26/04/2016.
 */
public class Story {
    private Page[] mPages;   //array of pages

    public Story() {
        mPages = new Page[9];

            mPages[0] = new Page(
                    R.drawable.page0,
                    "Hi im trying to find my way home can you help me %1$s, you're my only hope. Which way should I go to get home??\"",
                    new Choice("Take the Left track", 1),
                    new Choice("Take the right track", 2));

            mPages[1] = new Page(
                    R.drawable.page1,
                    "O my this way home is windy!!! I'll never get home if this wind keeps up. I need to find a different way to get home",
                    new Choice("Try the snowy way", 3),
                    new Choice("Try the wet way", 4));

            mPages[2] = new Page(
                    R.drawable.page2,
                    "O no this way is very rainy. If im not careful all of my paper work will get wet and my bosses will be mad with me!! I need to get out of here quick!",
                    new Choice("Try the wet way", 4),
                    new Choice("Try the hot way", 5));

            mPages[3] = new Page(
                    R.drawable.page3,
                    "This snow is hard to walk through don't you think %1$s. With any luck the route home won't get any harder",
                    new Choice("Head in to the city", 6),
                    new Choice("Take the scenic route", 7));

            mPages[4] = new Page(
                    R.drawable.page4,
                    "Well this is ridiculous! how am i supposed to get home when im swimming with the fishes %1$s.",
                    new Choice("Head in to the city", 6),
                    new Choice("Take the scenic route", 7));

            mPages[5] = new Page(
                    R.drawable.page5,
                    "This can't be the right way home its far too hot. I can't believe how much im sweating! %1$s maybe there is a less exhausting route i could take home.",
                    new Choice("Head in to the city", 6),
                    new Choice("Take the scenic route", 7));

            mPages[6] = new Page(
                    R.drawable.page6,
                    "I've made it to the city!! not to much further to go till im home now! I hope there is some thing in the fridge for me to eat when I get home. Perhaps there will be something for you to %1$s",
                    new Choice("This looks familiar", 8),
                    new Choice("Take the scenic route", 7));

            mPages[7] = new Page(
                    R.drawable.page7,
                    "This reminds me of little red riding hood, I hope there are no wolves about! Maybe I should pick up the pace a little to be on the safe side ",
                    new Choice("Head in to the city", 6),
                    new Choice("This looks familiar", 8));

            mPages[8] = new Page(
                    R.drawable.page8,
                    "Finally I have arrived. Thank you %1$s, you've helped me get home. I am so exhausted from my journey I need to lay down! Maybe I should buy a bike?");
        }

        public Page getPage(int pageNumber) {
            return mPages[pageNumber];
        }

    }





